#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`

#include <Adafruit_Sensor.h>
#include <DHT.h>

#define DHTPIN            12         // Pin which is connected to the DHT sensor.

#define OLED_RESET 4
#define DHTTYPE           DHT22


SSD1306Wire  Display1(0x3d, D2, D1);
SSD1306Wire  Display2(0x3c, D2, D1);

DHT dht(DHTPIN, DHTTYPE);

const int soilInPin = A0;  // Analog input pin that the potentiometer is attached to

int soilValue = 0;        // value read from the pot

int soilMapped = 0;        // value read from the pot

uint32_t delayMS;



const int ledPin =  LED_BUILTIN;// the number of the LED pin

// Variables will change:
int ledState = LOW;             // ledState used to set the LED

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time page was reset
unsigned long previousEyesMillis = 0;        // will store last time page was reset
unsigned long previousBlinkMillis = 0;        // will store last time page was reset


// Number of millis for each view
const long pageInterval = 10000;
const long eyeMoveInterval = 4000;     
const long eyeBlinkInterval = 3000;          


int whenMoveEyes, whenBlinkEyes = 0;
int view = 3;

int numViews = 3;


//the current position of the eyes is in current
//the target is where the eyes should be
//the target is updated at times given by whenMoveEyes.
//The future position is the position after the move and is currently calculated each time the eyese page is loaded.

struct point {
  int x = 0;
  int y = 0;
};

struct eyelid {
  int y = 0;
  int angle = 0;
};

struct point currentPos ;
struct point futurePos ;
struct point targetPos ;

struct eyelid lid ;


//Max eye speed
int v = 40;

void setup()   {                
  Serial.begin(9600);

  

  dht.begin();//Initialise the temerature and humidity Sensor.


  Display1.init();
  Display2.init();
  Display1.setI2cAutoInit(true);
  Display2.setI2cAutoInit(true);
  Display1.flipScreenVertically();
  Display2.flipScreenVertically();

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)


  pinMode(ledPin, OUTPUT);

    
  //LoadScreen();
  
}


void loop() {
  // Delay between measurements.
  // Wait a few milli seconds between measurements.
 delay(20);

  unsigned long currentMillis = millis();
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float humValue = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float tempValue = dht.readTemperature();

  soilValue = analogRead(soilInPin);
  
  soilMapped = map(soilValue, 650, 710, 100, 0);

//stuff that should happen when the page changes.
  if (currentMillis - previousMillis >= pageInterval) {
    previousMillis = currentMillis;//resets every page interval
    //swaps the current view every page Interval
    view += 1;
    //resets the page counter if max is reached
    if (view >= numViews){
      view = 0;
    }
    //initialise each page
    switch (view) {
    case 0:

      break;
      
    case 1:
      //any page initialisation needed for case 1
      break;
    }
  }

  //update the eyes position
  if (currentMillis - previousEyesMillis >= whenMoveEyes) {
    futurePos.x=random(10,118);
    futurePos.y=random(10,54);
    whenMoveEyes = random(500, 4000);
    targetPos.x = futurePos.x;
    targetPos.y = futurePos.y;
    previousEyesMillis = currentMillis;

  }
 
  //update the blink time position
  if (currentMillis - previousEyesMillis >= whenBlinkEyes) {
    whenBlinkEyes = random(700, 3000);
    lid.y = random(0,30);
    lid.angle = random(-10,10);

    //Begin Blink process and reset blink counter
    previousBlinkMillis = currentMillis;

  }



  switch (view) {
    case 0:
      currentPos = moveEyes(currentPos,  targetPos);
      eyesView(currentPos, lid);  
      break;
      
    case 1:
      dataView(humValue,tempValue, soilMapped);
      break;
    case 2:
      emojiView(humValue,tempValue, soilMapped);
      break;      
    case 3:
      welcomeView();
      break;            
    }

}
void dataView(float humValue,float tempValue, float soilMapped){

    Display1.setColor(WHITE);
    Display1.clear();
    Display1.setFont(ArialMT_Plain_16);
    Display1.drawString(0, 32, "Humidity: " + String(humValue));
    Display1.display();
    
    Display2.setColor(WHITE);
    Display2.clear();
    Display2.setFont(ArialMT_Plain_16);
    Display2.drawString(0, 0, "Soil: " + String(soilValue));
    Display2.drawString(0, 32, "Temp: " + String(tempValue));
    Display2.display();


}

void emojiView(float humValue,float tempValue, float soilMapped){
  if (soilMapped >= 50){
    //happy
    
  }
  else{
    //sad and dry
    
  }
    Display1.setColor(WHITE);
    Display1.clear();
    Display1.setFont(ArialMT_Plain_16);
    Display1.drawString(0, 16, "Humidity: " + String(humValue));
    Display1.display();
    
    Display2.setColor(WHITE);
    Display2.clear();
    Display2.setFont(ArialMT_Plain_16);
    Display2.drawString(0, 0, "Soil: " + String(soilValue));
    Display2.drawString(0, 32, "Temp: " + String(tempValue));
    Display2.display();

}

void welcomeView(){

    Display1.setColor(WHITE);
    Display1.clear();
    Display1.setFont(ArialMT_Plain_24);
    Display1.drawString(0, 0, "YOPA");
    Display1.display();
    
    Display2.setColor(WHITE);
    Display2.clear();
    Display2.setFont(ArialMT_Plain_24);
    Display2.drawString(0, 0, "2019");
    Display2.display();


}

void eyesView(struct point currentPos, struct eyelid lid){
    Display1.clear();
    Display1.setColor(WHITE);
    Display1.fillRect(0, 0, 128, 64);
    Display1.setColor(BLACK);
    Display1.fillCircle(currentPos.x, currentPos.y,  20);
    Display1.setColor(WHITE);
    Display1.fillCircle(currentPos.x-10, currentPos.y-10, 10);
    Display1.setColor(BLACK);
    Display1.fillRect(0, 0, 128, lid.y);
    Display1.display();
    
    Display2.clear();
    Display2.setColor(WHITE);
    Display2.fillRect(0, 0, 128, 64);
    Display2.setColor(BLACK);
    Display2.fillCircle(currentPos.x, currentPos.y,  20);
    Display2.setColor(WHITE);
    Display2.fillCircle(currentPos.x-10, currentPos.y-10, 10);
    Display2.setColor(BLACK);
    Display2.fillRect(0, 0, 128, lid.y);
    Display2.display();


}


struct point moveEyes(struct point currentPos, struct point targetPos){
  //This function limits the maximum speed of the eyes to v pixels.
  int dx = targetPos.x-currentPos.x;
  int dy = targetPos.y-currentPos.y;
  float dr = sqrt(sq(dx)+ sq(dy));
  float ratio =  v/dr;
  //either mover the eyes to the new position or proportionally if the target position is too far away.
  if (ratio >= 1){
    currentPos = targetPos;
  }
  else{
    currentPos.x = currentPos.x+(ratio*dx);
    currentPos.y = currentPos.y+(ratio*dy);
  }
  return currentPos;
}


